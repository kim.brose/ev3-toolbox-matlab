# RWTH - Mindstorms EV3 Toolbox for MATLAB

Hello, fellow LEGO constructors and MATLAB programmers, welcome to the RWTH - 
Mindstorms EV3 Toolbox for MATLAB! 

This toolbox was designed to simplify controlling LEGO MINDSTORMS EV3 
robots live using MATLAB. Via Bluetooth or USB you can easily read sensor values 
or control your robots’ motors. You can choose to either work on a high level, 
with a virtual twin of your EV3, or follow the EV3's firmware specification 
by sending commands directly. 

This toolbox is being developed at RWTH Aachen University, Germany. It is the 
successor of the 
[RWTH - Mindstorms NXT Toolbox](http://www.mindstorms.rwth-aachen.de/). Amongst other applications,
it is used in the yearly lab exercise ["MATLAB meets LEGO Mindstorms"](https://mindstorms.lfb.rwth-aachen.de)
by more than 400 students in the first year of their Eletrical Engineering curriculum at RWTH Aachen University


To get started, take a look at the installation instructions and examples below. 


## License

This software is distributed under the GNU Lesser General Public License v2.1 (LGPL 2.1).

This software is based on the [QUT EV3 MATLAB Toolkit](https://wiki.qut.edu.au/display/cyphy/QUT+EV3+MATLAB+toolkit) 
by Peter Corke et al. of Queensland University.  
Source: <https://github.com/petercorke/robotics-toolbox-matlab/tree/master/interfaces/EV3>  

*The QUT EV3 MATLAB Toolkit is part of the Robotics Toolbox for MATLAB, see:*  
*<https://github.com/petercorke/robotics-toolbox-matlab/>*  
*P. Corke, "Robotics, Vision \& Control: Fundamental Algorithms in Matlab", Springer, 2011, ISBN 978-3-642-20143*

This software uses the HIDAPI library by Alan Ott, Signal 11 Software, see <https://github.com/signal11/hidapi>

## How to quote

If you want to cite this software, you can use the following bibtex snippet:

 
```bibtex
@Misc{rwthmindstormsev3toolbox,
  author       = {Atorf, L. and Sondermann, B. and Stadtmann, T. and Rossmann, J.},
  title        = {RWTH - Mindstorms EV3 Toolbox},
  year         = {2018},
  version      = {1.0},
  organization = {Institute for Man-Machine Interaction, RWTH Aachen University},
  url          = {https://git.rwth-aachen.de/mindstorms/ev3-toolbox-matlab}
}
```


## Prerequisites 
If you use Windows, you will need to have the Instrument Control Toolbox 
installed for USB to work properly. 

This toolbox has been developed and tested on MATLAB 2014a/2016a, on Windows 
7/10 and Ubuntu 14.04/16.04. If you encounter problems using other 
configurations, feel free to contact us.

The EV3's firmware version needs to be updated to 1.09, bugs and unsuspected 
behaviour have been encountered when we used older versions.

## Installation 
In most cases, you will only need to download the toolbox and add its path to 
your MATLAB search path. ('Set path' -> 'Add with subfolders')

If you use a Linux system and connecting via Bluetooth does not work,
you can try to manually install the *HIDAPI*, an API for communication with
HID devices, which this toolbox uses for Bluetooth connection on Linux - a 
prebuilt version is delivered with the toolbox. HIDAPI can be installed using the 
[official repositories of your distribution](https://packages.ubuntu.com/de/source/trusty/hidapi/) 
or [built](https://github.com/signal11/hidapi/) on your own.


## Examples 
Before you can try out the following example, you will need to have your EV3
connected to your PC via Bluetooth or USB. If you use a Linux system and want
to try out Bluetooth, we suggest you use the 'btconnect'-script you find
in 'Tools'. It is a commandline script which aims to make pairing with the
brick more easy. The syntax is 'btconnect *brickName*' (you can read the 
name of your EV3 at the top of its display - it should be *EV3* by default).

```
% This example expects a motor at port A and a (random) sensor at port 1
b = EV3();
b.connect('usb'); % OR: b.connect('bt');
ma = b.motorA;
ma.setProperties('power', 50, 'limitValue', 720);
ma.start();
% fun
ma.waitFor();
disp(b.sensor1.value);
b.beep();
b.disconnect();
```
You can find more information on the toolbox functions/classes either by using 
*help classname.functionname* and *doc classname.functionname* in your MATLAB
editor (the *.functionname* is optional), or in the PDF/HTML documentation 
inside the subfolder *docs*.


## Building the documentation
The documentation is built using [Sphinx](http://www.sphinx-doc.org/en/stable/) and several
extensions: [matlabdomain](https://pypi.python.org/pypi/sphinxcontrib-matlabdomain), 
[Napoleon](https://pypi.python.org/pypi/sphinxcontrib-napoleon) and 
[Sphinx Bootstrap Theme](https://pypi.python.org/pypi/sphinx-bootstrap-theme/).

In order to setup your PC for building the documentation from the comments, you need to install these. 
Using Ubuntu, you can use the following instructions (for other distros/OS's checkout the websites above):
```
sudo apt-get install python-sphinx
pip install sphinxcontrib-napoleon
pip install -U sphinxcontrib-matlabdomain
pip install sphinx_bootstrap_theme
```

Note: If you get 'permission-denied' errors on the pip install lines, try adding 
'--user *your_username*' to each pip install line.

You also need to setup your latex parser if you want Sphinx to generate pdfs:
```
sudo apt install texlive-latex-base
sudo apt install texlive-latex-extra
```

The build process for different outputs/platforms is controlled with a Makefile.
In the doc-directory, use the following commands to generate html or pdf files. 
Other targets have not been tested.
```
make html
make latexpdf
```

## Acknowledgement

Special thanks to Tim Stadtmann for his effort and perseverance.


## Contact

Dipl.-Phys. Linus Atorf  
Institute for Man-Machine Interaction  
RWTH Aachen University  
Ahornstr. 55  
52074 Aachen, Germany   

Mail: atorf at mmi.rwth-aachen.de


## Disclaimer

This site is neither affiliated with nor endorsed by the LEGO Group. LEGO, MINDSTORMS, NXT, and EV3 are registered trademarks of the LEGO Group. 
