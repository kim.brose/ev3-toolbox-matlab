clear all

brickObject = EV3();
brickObject.connect('bt', 'serPort', '/dev/rfcomm0');

frate = 2.25;
dist = [];
ang = [];
ma = brickObject.motorA;
mb = brickObject.motorB;
ma.limitValue = 0; %Dummywert
mb.limitValue = 0; %Dummywert
ma.power = 30;
mb.power = -30;

ma.limitValue = 1000; %Dummywert
mb.limitValue = 1000; %Dummywert
ma.smoothStart = 30;
ma.smoothStop = 30;
mb.smoothStart = 30;
mb.smoothStop = 30;
ma.resetTachoCount;
mb.resetTachoCount;
ma.brakeMode = 'Brake';
mb.brakeMode = 'Brake';

ma.limitValue = 360*frate;
mb.limitValue = 360*frate;
ma.start();
mb.start();
pause(0.5);
while mb.isRunning
    ma.tachoCount
    a = ma.tachoCount / frate;
    d = brickObject.sensor4.value;
    ang = [ang a];
    dist = [dist d];
end
dist = min(150,dist);
polar(single(ang)/180.*pi,dist);