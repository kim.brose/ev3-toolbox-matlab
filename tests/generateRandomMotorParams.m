function [limitValue, limitMode, brakeMode, speedRegulation, power, syncedMode] = generateRandomMotorParams
%generateRandomMotorParams Generates random values for a motor
%   Generates and returns random values for tachoLimit, limitMode, brakeMode,
%   speedRegulation, power and syncedMode.


limitValue = int64(200 + 1000*rand());

limitModeNum = round(rand());
if limitModeNum == 0
    limitMode = 'Tacho';
else
    limitMode = 'Time';
end;

brakeModeNum = round(rand());
if brakeModeNum == 0
    brakeMode = 'Brake';
else
    brakeMode = 'Coast';
end;

speedRegulation = round(rand());

while true
    power = int64(rand()*200-100);
    if power <= -10 || power >= 10
        break;
    end%if
end%while

if speedRegulation == 0
    syncedMode = round(rand());
else
    syncedMode = 0;
end%if

end%function