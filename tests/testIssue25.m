b = EV3();
b.connect('bt', 'serPort', '/dev/rfcomm0');

motorR = b.motorA;
motorL = b.motorC;

motorR.setProperties('power', 100, 'limitValue', 850, 'speedRegulation', true);
motorL.setProperties('power', -100, 'limitValue', 850, 'speedRegulation', true);

motorL.start();
motorR.start();

pause(1);
motorL.waitFor();

motorR.speedRegulation = 'Off';

% move out of box
motorL.speedRegulation = 'Off'; % notwendig, sonst auch Fehlermeldung
while motorL.currentSpeed ~= 0
    pause(0.1); 
end

motorL.power = -motorL.power;
motorR.power = -motorL.power;

motorL.limitValue = 360;
motorL.brakeMode = 'brake';
motorL.syncedStart(motorR);