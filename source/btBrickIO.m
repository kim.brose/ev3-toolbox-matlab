classdef btBrickIO < BrickIO      
    % Bluetooth interface between MATLAB and the brick
    %
    % Notes:
    %     * Connects to the bluetooth module on the host through a serial
    %       connection. Hence be sure that a serial connection to the bluetooth
    %       module can be made. Also be sure that the bluetooth module has been paired
    %       to the brick before trying to connect.
    %     * Usage is OS-dependent:
    %           * Windows: the deviceName- & channel-properties are needed for connection. The implementation is based on the Instrument Control toolbox.
    %           * Linux (and potentially Mac): serialPort-property is needed for connection. The implementation is based on MATLAB's serial port implementation.
    %
    %     * For general information, see also :class:`BrickIO`.
    %
    % Attributes:
    %     debug (bool): If true, each open/close/read/write-call will be shown in the console.
    %        Defaults to false.
    %     serialPort (string): Path to the serial-port object. Only needed when using MATLAB's 
    %        serial class (i.e. on linux/mac). Defaults to '/dev/rfcomm0'.
    %     deviceName (string):  Name of the BT-device = the brick. Only needed when using the 
    %        Instrument Control toolbox (i.e. on windows). Defaults to 'EV3'.
    %     channel (numeric > 0): BT-channel of the connected BT-device. Only needed when using 
    %        the Instrument Control toolbox (i.e. on windows). Defaults to 1.
    %     timeOut (numeric >= 0): seconds after which a timeout-error occurs if no packet could be
    %        read. Defaults to 10.
    %     backend ('serial'|'instrumentControl'): Backend this implementation is based on. Is
    %        automatically chosen depending on the OS. Defaults to 'serial' on linux/mac
    %        systems, and to 'instrumentControl' on windows systems.
    %
    % ::
    % 
    %     Examples:
    %         % Connecting on windows 
    %         commHandle = btBrickIO('deviceName', 'MyEV3', 'channel', 1);
    %         % Connecting on windows using MATLABs default serial port implementation for testing 
    %         commHandle = btBrickIO('deviceName', 'MyEV3', 'channel', 1, 'backend', 'serial');
    %         % Connecting on mac/linux
    %         commHandle = btBrickIO('serPort', '/dev/rfcomm0');
    %
    
    properties
        % debug (bool): If true, each open/close/read/write-call will be noted in the console.
        %     Defaults to false.
        debug;
        % serialPort (string): Path to the serial-port object. Defaults to '/dev/rfcomm0'.
        %     Only needed when using MATLAB's serial class (i.e. on linux/mac). 
        serialPort;
        % deviceName (string):  Name of the BT-device = the brick. Defaults to 'EV3'.
        %     Only needed when using the Instrument Control toolbox (i.e. on windows). 
        deviceName;
        % channel (numeric > 0): BT-channel of the connected BT-device. Defaults to 1.
        %     Only needed when using the Instrument Control toolbox (i.e. on windows). 
        channel;
        % timeOut (numeric > 0): Seconds after which a timeout-error occurs if no packet could be read. 
        %     Defaults to 10.
        timeOut;
        % backend ('serial'|'instrumentControl'): Backend this implementation is based on. 
        %     Is automatically chosen depending on the OS. Defaults to 'serial' on linux/mac
        %     systems, and to 'instrumentControl' on windows systems.
        backend;
    end
    
    properties (Access = protected)
        % handle: Connection handle to the device
        handle;
    end 
    
    methods
        function brickIO = btBrickIO(varargin)
            % Create a btBrickIO object
            %
            % Arguments:
            %     varargin: Any number of property names as strings, each followed by the
            %         desired value. 
            % 
            % ::
            % 
            %     Examples: 
            %         % Connecting on windows
            %         commHandle = btBrickIO('deviceName', 'MyEV3', 'channel', 1); 
            %         % Connecting on windows using MATLABs default serial port implementation for testing
            %         commHandle = btBrickIO('deviceName', 'MyEV3', 'channel', 1, 'backend', 'serial'); 
            %         % Connecting on mac/linux 
            %         commHandle = btBrickIO('serPort', '/dev/rfcomm0');
            %
            % See also BTBRICKIO.SETPROPERTIES
            
            brickIO.setProperties(varargin{:});
            
            if brickIO.debug > 0
                fprintf('\t(DEBUG) (BT init) \n');
            end
            
            % Set the connection handle
            try
                if strcmp(brickIO.backend, 'serial')
                   brickIO.handle = serial(brickIO.serialPort);
                else
                   brickIO.handle = Bluetooth(brickIO.deviceName,brickIO.channel);
                end
            catch ME
                if ~isempty(strfind(ME.identifier, 'invalidPORT'))
                    % Throw a clean InvalidSerialPort to avoid confusion in upper layers
                    msg = 'Couldn''t connect to BT-device because given serial port is invalid.';
                    id = [ID(), ':', 'InvalidSerialPort'];
                    throw(MException(id, msg));
                else
                    % Throw combined error because error did not happen due to known reasons...
                    msg = 'Unknown error occurred while creating serial-port-object for BT connection.';
                    id = [ID(), ':', 'UnknownError'];
                    newException = MException(id, msg);
                    newException = addCause(newException, ME);
                    throw(newException);
                end
            end
            
            % Open the connection
            brickIO.open;
        end
        
        function delete(brickIO)
            % Delete the btBrickIO object and closes the connection
            
            if brickIO.debug > 0
                fprintf('\t(DEBUG) (BT delete) \n');
            end
            
            % Disconnect
            try
                brickIO.close;
            catch
                % Connection already closed (probably due to an error) - do nothing
            end
        end
        
        function open(brickIO)
            % Opens the bluetooth connection to the brick using fopen.
            
            if brickIO.debug > 0
                fprintf('\t(DEBUG) (BT open)\n');
            end
            
            % Open the bt handle
            try
                fopen(brickIO.handle);
            catch ME 
                if strcmp(ME.identifier, 'MATLAB:serial:fopen:opfailed')
                    % Throw only clean CommError to avoid confusion in upper layers
                    msg = 'Failed to open connection to Brick via Bluetooth.';
                    id = [ID(), ':', 'CommError'];
                    throw(MException(id, msg));
                else
                    % Throw combined error because error did not happen due to communication
                    % failure
                    msg = 'Unknown error occurred while connecting to the Brick via Bluetooth.';
                    id = [ID(), ':', 'UnknownError'];
                    newException = MException(id, msg);
                    newException = addCause(newException, ME);
                    throw(newException);
                end
            end
        end

        function close(brickIO)
            % Closes the bluetooth connection the brick using fclose.
            
            if brickIO.debug > 0
                fprintf('\t(DEBUG) (BT close) \n');
            end 
            
            try
                % Close the close handle
                fclose(brickIO.handle);
            catch ME
                % Throw combined error because error did not happen due to communication
                % failure
                msg = 'Unknown error occurred while disconnecting from the Brick via Bluetooth.';
                id = [ID(), ':', 'UnknownError'];
                newException = MException(id, msg);
                newException = addCause(newException, ME);
                throw(newException);
            end
        end
        
        function rmsg = read(brickIO)
            % Reads data from the brick through bluetooth via fread and returns the data in uint8 format.
            
            if brickIO.debug > 0
                fprintf('\t(DEBUG) (BT read) \n');
            end 
            
            try
                % Get the number of bytes to be read from the bt handle
                nLength = fread(brickIO.handle,2);

                % Read the remaining bytes
                rmsg = fread(brickIO.handle,double(typecast(uint8(nLength),'uint16')));
            catch ME
                if strcmp(ME.identifier, 'MATLAB:serial:fread:opfailed')
                    % Throw only clean CommError to avoid confusion in upper layers
                    msg = 'Failed to read data from Brick via Bluetooth.';
                    id = [ID(), ':', 'CommError'];
                    throw(MException(id, msg));
                else
                    % Throw combined error because error did not happen due to known reasons...
                    msg = 'Unknown error occurred while reading data from the Brick via BT.';
                    id = [ID(), ':', 'UnknownError'];
                    newException = MException(id, msg);
                    newException = addCause(newException, ME);
                    throw(newException);
                end
            end
            
            % Append the reply size to the return message
            rmsg = uint8([nLength' rmsg']);
        end
        
        function write(brickIO,wmsg)
            % Writes data to the brick through bluetooth via fwrite.
            %
            % Arguments:
            %     wmsg (uint8 array): Data to be written to the brick via bluetooth
            
            if brickIO.debug > 0
                fprintf('\t(DEBUG) (BT write) \n');
            end 
            
            try
                % Write to the bluetooth handle
                fwrite(brickIO.handle,wmsg);
            catch ME
                if strcmp(ME.identifier, 'MATLAB:serial:fwrite:opfailed')
                    % Throw only clean CommError to avoid confusion in upper layers
                    msg = 'Failed to send data to Brick via Bluetooth.';
                    id = [ID(), ':', 'CommError'];
                    throw(MException(id, msg));
                else
                    % Throw combined error because error did not happen due to known reasons...
                    msg = 'Unknown error occurred while sending data to the Brick via BT.';
                    id = [ID(), ':', 'UnknownError'];
                    newException = MException(id, msg);
                    newException = addCause(newException, ME);
                    throw(newException);
                end
            end
        end
        
        function set.timeOut(brickIO, timeOut)
            if ~isnumeric(timeOut) || timeOut < 0
                error(ID(), 'timeOut-period of Bluetooth-handle can only be set to positive numerical values.'); 
            end
            
            brickIO.timeOut = timeOut;
            
            if timeOut == 0
               brickIO.handle.Timeout = 9999999; % MATLAB.serial seems to have no option to disable timeout
            end
        end
        function set.backend(brickIO, backend)
            if ~strcmp(backend, 'serial')
                if ~strcmp(backend, 'instrumentControl')
                    msg = 'btBrickIO''s backend-parameter has to be either ''serial'' or ''instrumentControl''.';
                    id = [ID(), ':', 'InvalidParameter'];
                    throw(MException(id, msg));
                elseif ~license('test', 'instr_control_toolbox')
                    msg = 'Cannot use Instrument-Control-Toolbox for Bluetooth: Toolbox not installed.';
                    id = [ID(), ':', 'AssetNotAvailable'];
                    throw(MException(id, msg));
                end
            end
            
            brickIO.backend = backend;
        end
        function setProperties(brickIO, varargin)
            % Sets multiple btBrickIO properties at once using MATLAB's inputParser.
            %
            % The syntax is as follows: commHandle.setProperties('propertyName1',
            % propertyValue1, 'propertyName2', propertyValue2, ...). Valid, optional properties
            % are: debug, serPort, deviceName, channel, timeout.
            %
            % See also BTBRICKIO.DEBUG, BTBRICKIO.SERIALPORT, BTBRICKIO.DEVICENAME,
            % BTBRICKIO.CHANNEL, BTBRICKIO.TIMEOUT
            
            p = inputParser();
            p.KeepUnmatched = 1;
            
            p.addOptional('debug', false);
            p.addOptional('serPort', '/dev/rfcomm0');
            p.addOptional('deviceName', 'EV3');
            p.addOptional('channel', 1);
            p.addOptional('timeOut', 10);
            if(ispc && license('test', 'instr_control_toolbox'))  % Choose 'backend'-default depending on OS
                p.addOptional('backend', 'instrumentControl');
            else
                if ispc
                    warning('Instrument&Control-Toolbox not installed. Using MATLAB default backend for BT communication. This could lead to errors on Windows!'); 
                end
                p.addOptional('backend', 'serial');
            end
            
            p.parse(varargin{:});
            
            brickIO.debug = p.Results.debug;
            brickIO.serialPort = p.Results.serPort;
            brickIO.deviceName = p.Results.deviceName;
            brickIO.channel = p.Results.channel;
            brickIO.timeOut = p.Results.timeOut;
            brickIO.backend = p.Results.backend;
        end
    end 
end
