function mode = DeviceMode(type, modeNo)
% Converts a mode number to corresponding mode-enum in given type.
    if ~strcmp(class(modeNo), 'uint8')
        error('MATLAB:RWTHMindstormsEV3:noclass:DeviceMode:invalidInputType',...
            'Argument ''modeNo'' is of type ''%s''. Valid types are: uint8.', ...
            class(modeNo));
    end
    
    try
        % Motors are a special case where the mode name does not equal the type name
        if type == DeviceType.LargeMotor || type == DeviceType.MediumMotor
            mode = DeviceMode.Motor(modeNo);
        elseif type == DeviceType.Unknown || type == DeviceType.None || type == DeviceType.Error
            mode = DeviceMode.Default.Undefined;
        else
            mode = DeviceMode.(char(type))(modeNo);
        end
    catch ME
        error('ModeNo ''%d'' not valid for given type.', modeNo);
    end
end

