classdef BrickIO < MaskedHandle
    % Abstract class definition for brick input output
    %
    % Notes:
    %     * The read/write-methods use uint8-arrays.
    %     * Instances of child-classes of BrickIO represent connection handles. The connection is
    %       opened when creating them and closed when deleting them.
    %
    % Methods:
    %     open: Open the connection to the brick
    %     close: Close the connection to the brick
    %     read: Read data from the brick
    %     write: Write data to the brick
    % 
    
    properties (Abstract)
        % timeOut: time-out period (if 0, no time-out)
        timeOut 
    end
    
    properties (Abstract, Access = protected)
        % handle: Connection handle to the device
        handle
    end
    
    methods (Abstract)
        % Open the brick connection
        open(BrickIO)
        % Close the brick connection
        close(BrickIO)
        % Read data from the brick
        read(BrickIO)
        % Write data to the brick
        write(BrickIO)
    end
end