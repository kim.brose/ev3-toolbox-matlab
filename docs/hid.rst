.. automodule:: source

.. |br| raw:: html
   
   <br />
hidapi
======

.. autoclass:: hidapi
   :members: open, close, read, read_timeout, write, getHIDInfoString, setNonBlocking, init, exit, error, enumerate, getManufacturersString, getProductString, getSerialNumberString
