.. automodule:: source

.. |br| raw:: html
   
   <br />
usbBrickIO
==========

.. autoclass:: usbBrickIO
   :members: open, close, read, write, setProperties
